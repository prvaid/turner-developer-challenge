﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tites4Turner.Controllers
{
    public class HomeController : Controller
    {
        Models.TitlesContext _titlemodels = new Models.TitlesContext();

        public ActionResult Index(string searchTitle = null)
        {
            var model = _titlemodels.Titles.Where(r => searchTitle == null || (r.TitleName.ToLower()).Contains(searchTitle.ToLower())).ToList();

            return View(model);
        }
    }
}
